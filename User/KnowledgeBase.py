from typing import List, Tuple, Iterable
from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import MapProxy, GameObjectProxy, GameUncertaintyProxy
from ExpertSystem.Business.UserFramework import IKnowledgeBase
from ExpertSystem.Structure.RuleBase import Fact
from OrodaelTurrim.Structure.Enums import TerrainType, AttributeType, EffectType, GameRole, GameObjectType
from OrodaelTurrim.Structure.Position import OffsetPosition, CubicPosition, AxialPosition, Position


class MembershipFunction:
    """
    Represents a membership function for fuzzy logic.
    When called with parameter x, returns value mu(x).
    """
    def __init__(self,
                 name: str,
                 values: List[Tuple[int, float]]):
        self.name = name
        self.values = sorted(values, key=lambda bp: bp[0])

    def __call__(self, at) -> float:
        try:
            before = next(x for x in reversed(self.values) if x[0] < at)
        except StopIteration:
            return self.values[0][1]
        try:
            after = next(x for x in self.values if x[0] > at)
        except StopIteration:
            return self.values[-1][1]
        # interpolation between the two points
        #     y0(x1-x)+y1(x-x0)
        # y = -----------------
        #           x1-x0
        return (before[1] * (after[0] - at) + after[1] * (at - before[0])) / (after[0] - before[0])

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return f'<{self.__class__.__name__}: \'{self.name}\'>'


class FuzzyProcessor:
    """
    Wraps together several fuzzy sets.
    When called with a crisp value, determines which function has the maximal value at that point
    and returns its linguistic variable.
    """
    def __init__(self, *args: MembershipFunction):
        self.funcs = args

    def __call__(self, at) -> str:
        res = max(self.funcs, key=lambda func: func(at))
        return res.name

    def __repr__(self):
        return f'<{self.__class__.__name__}: {",".join(f.name for f in self.funcs)}>'


class KnowledgeBase(IKnowledgeBase):
    """
    Class for defining known facts based on Proxy information. You can transform here any information from
    proxy to better format of Facts. Important is method `create_knowledge_base()`. Return value of this method
    will be passed to `Inference.interfere`. It is recommended to use Fact class but you can use another type.

    |
    |
    | Class provides attributes:

    - **map_proxy [MapProxy]** - Proxy for access to map information
    - **game_object_proxy [GameObjectProxy]** - Proxy for access to all game object information
    - **uncertainty_proxy [UncertaintyProxy]** - Proxy for access to all uncertainty information in game
    - **player [PlayerTag]** - class that serve as instance of user player for identification in proxy methods

    """
    map_proxy: MapProxy
    game_object_proxy: GameObjectProxy
    game_uncertainty_proxy: GameUncertaintyProxy
    player: PlayerTag

    fuzzy_money = FuzzyProcessor(
        MembershipFunction('low', [(15, 1), (30, 0)]),
        MembershipFunction('mid', [(20, 0), (30, 1), (50, 1), (60, 0)]),
        MembershipFunction('high', [(50, 0), (65, 1)])
    )

    fuzzy_defence = FuzzyProcessor(
        MembershipFunction('low', [(6, 1), (8, 0)]),
        MembershipFunction('mid', [(5, 0), (9, 1), (11, 1), (13, 0)]),
        MembershipFunction('high', [(10, 0), (13, 1)])
    )

    def __init__(self, map_proxy: MapProxy, game_object_proxy: GameObjectProxy,
                 game_uncertainty_proxy: GameUncertaintyProxy, player: PlayerTag):
        """
        You can add some code to __init__ function, but don't change the signature. You cannot initialize
        KnowledgeBase class manually so, it is make no sense to change signature.
        """
        super().__init__(map_proxy, game_object_proxy, game_uncertainty_proxy, player)

    def create_knowledge_base(self) -> List[Fact]:
        """
        Method for create user knowledge base. You can also have other class methods, but entry point must be this
        function. Don't change the signature of the method, you can change return value, but it is not recommended.
        """

        facts = [Fact('base_spot', eval_function=self.find_base_spot, data=self.find_base_spot),
                 Fact('player_has_base', eval_function=self.has_base),
                 Fact('base_position', eval_function=self.base_position),
                 Fact('base_defence', data=self.get_fuzzy_defence_level),
                 Fact('money', eval_function=self.get_fuzzy_money),
                 Fact('money_crisp', eval_function=self.get_crisp_money, data=self.get_crisp_money)]

        return facts

    def base_position(self):
        return self.map_proxy.get_bases_positions().pop()

    def has_base(self, val='True'):
        value = {
            'true': True,
            'false': False
        }
        return value[val.lower()] == self.map_proxy.player_have_base(self.player)

    def get_crisp_money(self):
        return self.game_object_proxy.get_resources(self.player)

    def get_fuzzy_money(self):
        money_crisp = self.game_object_proxy.get_resources(self.player)
        return self.fuzzy_money(money_crisp)

    def area_generator(self, pos: Position) -> Iterable:
        diameter = 1
        for i in range(-diameter, diameter + 1):
            for j in range(-diameter, diameter + 1):
                n = OffsetPosition(pos.offset.q + i, pos.offset.r + j)
                if self.map_proxy.is_position_on_map(n):
                    yield n

    def get_crisp_defence(self) -> int:
        base = self.map_proxy.get_bases_positions().pop()
        return sum(1 for x in self.area_generator(base)
                   if self.map_proxy.is_position_occupied(x)
                   and self.game_object_proxy.get_object_type(x) in GameObjectType.defenders())

    def get_fuzzy_defence_level(self):
        return self.fuzzy_defence(self.get_crisp_defence())

    def visible_free_tile(self, terrain_type: str):
        """ Find random free tile with given terrain type """
        tiles = self.map_proxy.get_player_visible_tiles()
        border_tiles = self.map_proxy.get_border_tiles()

        for position in tiles:
            terrain = self.map_proxy.get_terrain_type(position) == TerrainType.from_string(terrain_type)
            occupied = self.map_proxy.is_position_occupied(position)
            if terrain and not occupied and position not in border_tiles:
                return position
        return None

    def neighbours_generator(self, pos: OffsetPosition, diameter: int):
        for i in range(-diameter, diameter + 1):
            for j in range(-diameter, diameter + 1):
                n = OffsetPosition(pos.q + i, pos.r + j)
                if self.map_proxy.is_position_on_map(n):
                    yield n

    def neighbours_def_generator(self, pos: OffsetPosition) -> Iterable:
        """Generates def bonuses of all tiles in an area"""
        for n in self.neighbours_generator(pos, 1):
            try:
                yield (getattr(self.map_proxy.get_terrain_type(n).value, 'affect_defense')(1) - 1) * 100
            except AttributeError:
                pass

    def calculate_def_bonus(self, pos: OffsetPosition) -> int:
        return sum(x for x in self.neighbours_def_generator(pos))

    def find_base_spot(self) -> OffsetPosition:
        best = OffsetPosition(0, 0)
        max_value = 0
        for x in range(self.map_proxy.get_map_width()):
            for y in range(self.map_proxy.get_map_height()):
                pos = OffsetPosition(x, y)
                value = self.calculate_def_bonus(pos)
                if value > max_value:
                    best = pos
                    max_value = value
        return best
