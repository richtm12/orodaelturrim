from typing import Iterable, Tuple, Iterator

from OrodaelTurrim.Business.Interface.Player import PlayerTag
from OrodaelTurrim.Business.Proxy import GameControlProxy
from ExpertSystem.Business.UserFramework import IActionBase
from OrodaelTurrim.Structure.Enums import GameObjectType
from OrodaelTurrim.Structure.Filter.AttackFilter import AttackMostVulnerableFilter, AttackNearestFilter
from OrodaelTurrim.Structure.Filter.Factory import FilterFactory
from OrodaelTurrim.Structure.GameObjects.GameObject import SpawnInformation
from OrodaelTurrim.Structure.Position import OffsetPosition
from OrodaelTurrim.Structure.Exceptions import IllegalActionException


class ActionBase(IActionBase):
    """
    You can define here your custom actions. Methods must be public (not starting with __ or _) and must have unique
    names. Methods could have as many arguments as you want. Instance of this class will be available in
    Inference class.

    **This class provides:**

    * self.game_control_proxy [GameControlProxy] for doing actions in game
    * self.player [PlayerTag] instance of your player for identification yourself in proxy

    Usage of ActionBase is described in documentation.
    """
    game_control_proxy: GameControlProxy
    player: PlayerTag

    def build_base(self, base_spot: OffsetPosition):
        print(f'Building base at {base_spot.offset.q}:{base_spot.offset.r}, {self.map_proxy.get_terrain_type(base_spot)}')
        nearest = FilterFactory().attack_filter(AttackNearestFilter)
        self.game_control_proxy.spawn_unit(
            SpawnInformation(self.player,
                             GameObjectType.BASE,
                             base_spot,
                             [nearest], []))

    def build_unit(self, unit, visible_free_tile):
        print(f'Building {unit} at {visible_free_tile.q}:{visible_free_tile.r}, {self.map_proxy.get_terrain_type(visible_free_tile)}')
        mv_filter = FilterFactory().attack_filter(AttackMostVulnerableFilter)
        self.game_control_proxy.spawn_unit(
            SpawnInformation(self.player,
                             getattr(GameObjectType, unit),
                             visible_free_tile,
                             [mv_filter], []))

    @staticmethod
    def spiral(pos: OffsetPosition) -> Iterator[OffsetPosition]:
        """Yields tiles in a spiral, starting at passed center."""
        def offset(start: OffsetPosition, direction: Tuple[int, int]):
            return OffsetPosition(start.q + direction[0], start.r + direction[1])
        right = (1, 0)
        down = (0, 1)
        left = (-1, 0)
        up = (0, -1)
        i = 1
        while True:
            for _ in range(i):
                pos = offset(pos, right)
                yield pos
            for _ in range(i):
                pos = offset(pos, down)
                yield pos
            i += 1
            for _ in range(i):
                pos = offset(pos, left)
                yield pos
            for _ in range(i):
                pos = offset(pos, up)
                yield pos
            i += 1

    def free_in_spiral(self, pos: OffsetPosition):
        """Yields tiles the player can build on in a spiral starting at pos."""
        spiral = self.spiral(pos)
        while True:
            i = next(spiral)
            if self.map_proxy.is_position_on_map(i)\
                    and not self.map_proxy.is_position_occupied(i)\
                    and i not in self.map_proxy.get_border_tiles():
                yield i

    def build_guards(self, units: str):
        units = int(units)
        base = self.map_proxy.get_bases_positions().pop()
        f = FilterFactory().attack_filter(AttackNearestFilter)
        for t in base.get_all_neighbours():
            if self.map_proxy.is_position_on_map(t) \
                    and not self.map_proxy.is_position_occupied(t) \
                    and t not in self.map_proxy.get_border_tiles():
                units -= 1
                try:
                    self.game_control_proxy.spawn_unit(
                        SpawnInformation(self.player,
                                         GameObjectType.KNIGHT,
                                         t,
                                         [f], []))
                    print(f'Building knight at {t.offset.q}:{t.offset.r}')
                except IllegalActionException as e:
                    # print(e)
                    return
        tiles = self.free_in_spiral(base.offset)
        while units > 0:
            try:
                where = next(tiles)
                self.game_control_proxy.spawn_unit(
                    SpawnInformation(self.player,
                                     GameObjectType.KNIGHT,
                                     where,
                                     [f], []))
                print(f'Building knight at {where.offset.q}:{where.offset.r}')
            except StopIteration:
                break
            except IllegalActionException as e:
                # print(e)
                break

